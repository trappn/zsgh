#!/bin/sh
osascript -- - "$1" "$2" <<'EOF'
  on run(argv)
    #return display notification item 1 of argv
    tell application "Terminal"
      reopen
      activate
      do script "cd " & quoted form of (item 1 of argv) & ";zsgh_gui " & quoted form of (item 2 of argv) in window 1
    end tell
  end run
EOF

