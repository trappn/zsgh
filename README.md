<pre>
 ______     ______     ______     __  __    
/\___  \   /\  ___\   /\  ___\   /\ \_\ \        The Zürich Space Group Helper:  
\/_/  /__  \ \___  \  \ \ \__ \  \ \  __ \         A free, didactic tool for     
  /\_____\  \/\_____\  \ \_____\  \ \_\ \_\        space group determination  
  \/_____/   \/_____/   \/_____/   \/_/\/_/        using systematic absences.            
                                            
 (c) 2017-2023 M. Solar, N. Trapp, M. Wörle
 Small Molecule Crystallography Center @ ETH Zürich

 Repository:  https://gitlab.ethz.ch/trappn/zsgh
 References:  International Tables for Crystallography, https://it.iucr.org/,
                doi: 10.1107/97809553602060000001.
              R.W. Grosse-Kunstleve, N.K. Sauter, P.D. Adams, Acta Cryst. A
                (2004), 60, 1-6, doi: 10.1107/S010876730302186X.
              P. M. de Wolff, Comput. Math. Applic. (1988), 16(5-8), 487-492.
              P. M. de Wolff, Acta Cryst. A (1991), 47, 29-36, doi:
                10.1107/S0108767390009485.
              I. Krivy, B. Gruber, Acta Cryst. A (1976), 32, 297-298, doi:
                10.1107/S0567739476000636.
              B. Gruber, Acta Cryst. A (1973), 29, 433-440, doi:
                10.1107/S0567739473001063.
 </pre>

**Downloads:**
<br><br>most recent version: v1.9 (21.05.2024)

Windows Installer (64bit, Windows 7 or later):
https://gitlab.ethz.ch/trappn/zsgh/-/raw/master/bin/win64/ZSGH_win_1.9_210524.exe<br>
Linux Binaries (64bit):
https://gitlab.ethz.ch/trappn/zsgh/-/tree/master/bin/linux64<br>
MacOS Installer (64bit, 10.9 or later):
https://gitlab.ethz.ch/trappn/zsgh/-/raw/master/bin/mac/zsgh_installer.pkg<br>

Tutorial with example structure (not identical with ZSC2024 example):
https://gitlab.ethz.ch/trappn/zsgh/-/tree/master/examples/tutorial 

**Installation:**
<br><br>Installation videos showing Olex2 installation & integration of ShelX programs / ZSGH:<br><br>
Windows 11: https://youtu.be/BY5ScsG1jcs?si=TCJ7st1ox8ODjcI_<br>
MacOS Sequoia: https://youtu.be/ust1x_KoxiY?si=zSsRStmL_QuYjBJz

**Usage:**
```
usage: zsgh [-h] [-d] [STRUCTURE_ID]

positional arguments:
  STRUCTURE_ID

optional arguments:
  -h, --help    show this help message and exit
  -d            use darker terminal colors (for white background)
```

The program will look for a STRUCTURE_ID.hkl file in the directory
it is invoked in and present a matching list of files containing unit cell constants
in a selection menu. If it cannot find any, the user will be prompted.
From there on the process is guided by on-screen dialogue.

**Purpose:**

ZSGH was written with the intention of providing a free, simple and
universal space group determination tool to the general public. The commonly used
commercial tools such as XPREP (Bruker), XPLAIN (Rigaku) and GRAL (Rigaku Oxford Diffraction)
require a license or run only under Windows, thereby preventing use and distribution in lectures, workshops and schools.
Free tools are often part of clunky program suites, do not run on multiple platforms
and/or lack documentation.
Additionally, there was a demand for a more transparent and didactic process with
uncondensed output for better diagnostics. ZSGH very closely follows what a person
using the International Tables of Crystallography (ITC) would do, but with some
extra guidance. Although it is not strictly needed, we recommend having the ITC around.

Feel free to take any part of the code and implement it into your own software.
The implementation is minimum-dependency, (almost) pure Python3. Single file
commandline executables are provided.

Olex2 (http://www.olexsys.org/) version 1.3 and later recognizes the executable when
found somewhere in the system path or the Olex2 installation directory. This is automatically
the case when you use the Windows or MacOS installer. The program can then be invoked from
the Olex2 commandline by typing "zsgh", or via a hyperlink under the "Solve" tab.
See file Olex2_integration.txt for instructions using current Olex2 versions
(for MacOS and Linux additionally a macro must be changed).

Note that without access to the covariance matrix, we can only roughly estimate
transformed unit cell esds. If angles change significantly during transformation,
it is highly advisable to refinalize the data in the diffractometer software using
the correct cell setting to recalculate standard errors.

**Program features:**
- unit cell determination using systematic absence violations
- procedure very close to using ITC
- didactic / verbose procedures
- reads standard .hkl files and spots the most common formatting problems
- reads UC/formula/wavelength from .cif_od/.cif/.p4p/.res/.ins/.hkl files or user input
- Niggli reduction
- systematic absence counter
- reciprocal plane display
- semi-automatic space group determination (just press return a lot and ultimately you will end up with a space group)
- can transform into arbitrary alternate cell settings
- transforms HKL indices using final matrix
- Intensity statistics (including Rint & Rsym)
- automatic crystal system suggestion based on G6 vector & Rsym (in manual mode)
- E-value statistics & N(z) plots
- unfiltered listing of all absences in all directions (like Platon)
- Condensed intensity statistics for final Laue class
- writes SHELX compatible .ins and transformed .hkl file or .ins with HKLF4 matrix (leaving .hkl untouched)
- tight Olex2 integration
- complete log file
